$(document).ready(function() {
//open extracupo
    $(".extraCupo").click( function(){
        $(this).next(".zb-back-op").slideToggle();
        if ( $(this).hasClass("zb-img-bkg")) {
            $(this).removeClass("zb-img-bkg");
            $(this).addClass("zb-img-bkg-2");
        }
        else{
            $(this).removeClass("zb-img-bkg-2");
            $(this).addClass("zb-img-bkg");
        }
    });
	
  $('.pruebaBoton').click(function(){
    $('.pruebaBoton').addClass("btnLoading");
});
	
	//Estado de cuenta
	$('.zb-envio-mail .pageTitle').click( function(){
		if ($(this).hasClass('active')) {
			$('.zb-envio-mail form').hide();
			$(this).removeClass('active');
		} else {
			$('.zb-envio-mail form').show();
			$(this).addClass('active');
		}
	});
	$('#input-check-correo').click( function(){
		if ($(this).hasClass('active')) {
			$('#mensajes-estado-correo').hide();
			$(this).removeClass('active');
		} else {
			$('#mensajes-estado-correo').show();
			$(this).addClass('active');
		}
	});
	$('#email-campo img').click( function(){
		$(this).parent().hide();
		$('#email-campo-editar').show();
	});
	$('#actualizar-pref').click( function(){
		$('#div-clave-dinamica').show();
	});
	$('#confirmar-codigo').click( function(){
		$('#div-clave-dinamica').hide();
		$('#email-campo-editar').hide();
		$('#email-campo').show();
		$('.mensajes-confirmacion').addClass('exito');
		$('.mensajes-confirmacion').show();
	});
	$('#confirmar-codigo-error').click( function(){
		$('.mensajes-confirmacion').addClass('error');
		$('.mensajes-confirmacion').show();
	});
	
	$(document).keyup(function(e) {
	  if (e.keyCode === 13){
		if ($('.mensajes-confirmacion').is(':visible')){
			if ($('.mensajes-confirmacion').hasClass('exito')) {
				$('.mensajes-confirmacion').removeClass('exito');
			} else if ($('.mensajes-confirmacion').hasClass('error')) {
				$('.mensajes-confirmacion').removeClass('error');
			}
			$('.mensajes-confirmacion').hide();
		}
	  }
	  
	  if (e.keyCode === 27){		  
		if ($('.mensajes-confirmacion').is(':visible')){
			if ($('.mensajes-confirmacion').hasClass('exito')) {
				$('.mensajes-confirmacion').removeClass('exito');
			} else if ($('.mensajes-confirmacion').hasClass('error')) {
				$('.mensajes-confirmacion').removeClass('error');
			}
			$('.mensajes-confirmacion').hide();
		}
	  }
	});

    //close dropdown
    $("#close-modal").on('click', function() {
        $('#dd-modal').effect('drop');
    });

    //dropdown data
    var ddData = [{
        text: "ABC VISA",
        value: "t-abc",
        description: "xxxx-4236",
        imageSrc: "img/cards/abcVisa.png"
    }, {
        text: "ABC",
        value: "t-abcvisa",
        description: "xxxx-4236",
        imageSrc: "img/cards/abcCard.png"
    }, {
        text: "Abcdin",
        value: "t-abcdin",
        description: "xxxx-2644",
        imageSrc: "img/cards/abcdinCard.png"
    }, {
        text: "Din",
        value: "t-din",
        description: "xxxx-1853",
        imageSrc: "img/cards/dinCard.png"
    }];

    //activar dropdown
    $('#selectorTarjetas').ddslick({
        data: ddData,
        imagePosition: "left",

    });

    //slidebars
    $.slidebars();

    //tabs
    $('ul.tabs').each(function() {

        var $active, $content, $links = $(this).find('a');

        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active.attr('href'));

        $links.not($active).each(function() {
            $($(this).attr('href')).hide();
        });

        $(this).on('click', 'a', function(e) {
            var c = this;
            $active.removeClass('active');
            $content.fadeOut("fast", function() {
                $active = $(c);
                $content = $($(c).attr('href'));

                $active.addClass('active');
                $content.fadeIn("slow");
            });
            e.preventDefault();
        });
    });



// TABLE COLORS
$(function() {
    $('.table').footable();
    $('.footable-visible').click(function(e) {
        $('table tr:nth-child(odd)').removeClass('odd');
        $('table tr:nth-child(odd)').addClass('odd');
    });

    $('.sort-column').click(function(e) {
        e.preventDefault();

        //get the footable sort object
        var footableSort = $('table').data('footable-sort');

        //get the index we are wanting to sort by
        var index = $(this).data('index');

        footableSort.doSort(index, 'toggle');
    });
});

	
	
	
	
	



});

